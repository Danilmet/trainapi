<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Exercise
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string|null $description
 * @property string $approaches
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $type_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Training[] $trainings
 * @property-read \App\ExerciseType $type
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereApproaches($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereUserId($value)
 */
	class Exercise extends \Eloquent {}
}

namespace App{
/**
 * App\ExerciseType
 *
 * @property int $id
 * @property string $name
 * @property string $display_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Exercise[] $exercises
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseType whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseType whereName($value)
 */
	class ExerciseType extends \Eloquent {}
}

namespace App{
/**
 * App\Training
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $color
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Exercise[] $exercises
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Training newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Training newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Training query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Training whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Training whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Training whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Training whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Training whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Training whereUserId($value)
 */
	class Training extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $api_token
 * @property string $name
 * @property string $gender
 * @property int $weight
 * @property int $height
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Exercise[] $exercises
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Training[] $trainings
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereWeight($value)
 */
	class User extends \Eloquent {}
}

