<?php

namespace App\Observers;

use App\Exercise;
use Auth;

class ExerciseObserver
{
	/**
	 * @param Exercise $exercise
	 */
	public function creating(Exercise $exercise)
	{
		$exercise->user_id = Auth::user()->id;
    }
}
