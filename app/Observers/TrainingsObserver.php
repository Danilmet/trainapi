<?php

namespace App\Observers;

use App\Training;
use Auth;

class TrainingsObserver
{
	/**
	 * @param Training $exercise
	 */
	public function creating(Training $exercise)
	{
		$exercise->user_id = Auth::user()->id;
	}
}
