<?php

namespace App\Providers;

use App\Exercise;
use App\Policies\ExercisePolicy;
use App\Policies\TrainingPolicy;
use App\Training;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
		Exercise::class => ExercisePolicy::class,
		Training::class => TrainingPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
