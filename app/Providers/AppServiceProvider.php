<?php

namespace App\Providers;

use App\Exercise;
use App\Observers\ExerciseObserver;
use App\Observers\TrainingsObserver;
use App\Training;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Exercise::observe(ExerciseObserver::class);
        Training::observe(TrainingsObserver::class);
    }
}
