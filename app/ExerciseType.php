<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ExerciseType extends Model
{
    /**
	 * Relations
	 */

	/**
	 * @return HasMany
	 */
	public function exercises(): HasMany
	{
		return $this->hasMany(Exercise::class);
    }
}
