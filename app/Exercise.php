<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Exercise extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'description',
		'approaches',
		'type_id',
	];

	/**
	 * Relations
	 */

	/**
	 * @return BelongsTo
	 */
	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @return BelongsToMany
	 */
	public function trainings(): BelongsToMany
	{
		return $this->belongsToMany(Training::class);
	}

	/**
	 * @return BelongsTo
	 */
	public function type(): BelongsTo
	{
		return $this->belongsTo(ExerciseType::class);
	}
}
