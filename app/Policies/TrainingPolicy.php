<?php

namespace App\Policies;

use App\Training;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TrainingPolicy
{
    use HandlesAuthorization;

	public function update(User $user, Training $training)
	{
		return $user->id === $training->user_id;
	}
}
