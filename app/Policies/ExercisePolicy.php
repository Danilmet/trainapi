<?php

namespace App\Policies;

use App\Exercise;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExercisePolicy
{
    use HandlesAuthorization;

	public function update(User $user, Exercise $exercise)
	{
		return $user->id === $exercise->user_id;
	}
}
