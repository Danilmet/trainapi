<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\User;
use DB;
use Exception;
use Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Str;

class AuthController extends Controller
{
	/**
	 * @param LoginRequest $request
	 * @return JsonResponse
	 */
	public function login(LoginRequest $request): JsonResponse
	{
		$credentials = $request->only('login', 'password');

		$user = User::whereLogin($credentials['login'])->first();

		if (Hash::check($credentials['password'], $user->password)) {
			return response()->json([
				'api_token' => $user->api_token,
				'user' => new UserResource($user),
			]);
		} else {
			return response()->json([
				'message' => 'Неверный логин или пароль!'
			], 422);
		}
	}

	/**
	 * @param RegisterRequest $request
	 * @return JsonResponse
	 */
	public function register(RegisterRequest $request): JsonResponse
	{
		$credentials = $request->all();
		$credentials['password'] = Hash::make($credentials['password']);
		$credentials['api_token'] = Str::random(60);
		$credentials['remember_token'] = Str::random(10);
		$credentials['email_verified_at'] = now();

		DB::beginTransaction();

		try {
			$user = User::create($credentials);

			DB::commit();

		} catch (Exception $exception) {

			DB::rollBack();

			return response()->json([
				'message' => 'Ошибка регистрации',
				'error' => $exception->getMessage(),
				'trace' => $exception->getTrace(),
			]);
		}

		return response()->json([
			'api_token' => $user->api_token,
			'user' => new UserResource($user),
		]);
	}
}
