<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UpdatePropertiesRequest;
use App\Http\Resources\UserResource;
use Auth;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return UserResource
	 */
	public function index(): UserResource
	{
		$user = Auth::user();

		return new UserResource($user);
	}

	/**
	 * @param UpdatePropertiesRequest $request
	 * @return JsonResponse
	 */
	public function updateProperties(UpdatePropertiesRequest $request): JsonResponse
	{
		DB::beginTransaction();

		try {
			$user = Auth::user();

			$user->weight = $request->input('weight');
			$user->height = $request->input('height');

			$user->save();

			DB::commit();
		} catch (Exception $exception) {
			DB::rollBack();

			return response()->json([
				'message' => 'Ошибка изменения параметров Пользователя',
				'error' => $exception->getMessage(),
				'trace' => $exception->getTrace(),
			], 400);
		}

		return response()->json([
			'message' => 'Параметры изменены',
		], 201);
    }
}
