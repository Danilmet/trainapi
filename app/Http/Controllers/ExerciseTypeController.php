<?php

namespace App\Http\Controllers;

use App\ExerciseType;
use App\Http\Resources\ExerciseTypeResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ExerciseTypeController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return JsonResponse
	 */
    public function index(): JsonResponse
    {
        return ExerciseTypeResource::collection(ExerciseType::all())->response();
    }
}
