<?php

namespace App\Http\Controllers;

use App\Exercise;
use App\Http\Requests\Exercises\StoreRequest;
use App\Http\Requests\Exercises\UpdateRequest;
use App\Http\Resources\ExerciseResource;
use Auth;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ExerciseController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return JsonResponse
	 */
    public function index(): JsonResponse
    {
    	$exercises = Auth::user()->exercises;

        return ExerciseResource::collection($exercises)->response();
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param StoreRequest $request
	 * @return JsonResponse
	 * @throws Exception
	 */
    public function store(StoreRequest $request): JsonResponse
    {
    	DB::beginTransaction();

    	try {
    		$exercise = Exercise::create($request->all());

    		DB::commit();
		} catch (Exception $exception) {
    		DB::rollBack();

    		return response()->json([
    			'message' => 'Ошибка сохранения Упражнения',
				'error' => $exception->getMessage(),
				'trace' => $exception->getTrace(),
			], 400);
		}

		return response()->json([
			'message' => 'Упражнение сохранено',
			'id' => $exercise->id,
		], 201);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Exercise $exercise
	 * @return ExerciseResource
	 */
    public function show(Exercise $exercise): ExerciseResource
    {
        return new ExerciseResource($exercise);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param UpdateRequest $request
	 * @param Exercise $exercise
	 * @return JsonResponse
	 * @throws Exception
	 */
    public function update(UpdateRequest $request, Exercise $exercise): JsonResponse
    {
    	DB::beginTransaction();

        try {
        	$exercise->update($request->all());

        	DB::commit();
		} catch (Exception $exception) {
        	DB::rollBack();

			return response()->json([
				'message' => 'Ошибка изменения Упражнения',
				'error' => $exception->getMessage(),
				'trace' => $exception->getTrace(),
			], 400);
		}

		return response()->json([
			'message' => 'Упражнение изменено',
		], 201);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Exercise $exercise
	 * @return JsonResponse
	 */
    public function destroy(Exercise $exercise): JsonResponse
    {
        try {
        	$exercise->delete();
		} catch (Exception $exception) {
			return response()->json([
				'message' => 'Ошибка удаления Упражения',
				'error' => $exception->getMessage(),
			], 400);
		}

		return response()->json([
			'message' => 'Упражнение удалено',
		], 200);
    }
}
