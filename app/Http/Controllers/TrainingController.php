<?php

namespace App\Http\Controllers;

use App\Http\Requests\Trainings\StroreRequest;
use App\Http\Requests\Trainings\UpdateRequest;
use App\Http\Resources\TrainingResource;
use App\Training;
use Auth;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TrainingController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return JsonResponse
	 */
    public function index(): JsonResponse
    {
        $trainings = Auth::user()->trainings;

        return TrainingResource::collection($trainings)->response();
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param StroreRequest $request
	 * @return JsonResponse
	 * @throws Exception
	 */
    public function store(StroreRequest $request): JsonResponse
    {
        DB::beginTransaction();

        try {
        	$training = Training::create($request->all());

        	$training->exercises()->sync($request->input('exercises') ?: null);

        	DB::commit();
		} catch (Exception $exception) {
        	DB::rollBack();

        	return response()->json([
        		'message' => 'Ошибка сохранения Тренировки',
				'error' => $exception->getMessage(),
				'trace' => $exception->getTrace(),
			], 400);
		}

		return response()->json([
			'message' => 'Тренировка сохранена',
			'id' => $training->id,
		], 201);
    }

	/**
	 * Display the specified resource.
	 *
	 * @param Training $training
	 * @return TrainingResource
	 */
    public function show(Training $training): TrainingResource
    {
        return new TrainingResource($training);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param UpdateRequest $request
	 * @param Training $training
	 * @return JsonResponse
	 * @throws Exception
	 */
    public function update(UpdateRequest $request, Training $training): JsonResponse
    {
        DB::beginTransaction();

        try {
        	$training->update($request->all());

        	$training->exercises()->sync($request->input('exercises') ?: null);

        	DB::commit();
		} catch (Exception $exception) {
        	DB::rollBack();

        	return response()->json([
				'message' => 'Ошибка изменения Тренировки',
				'error' => $exception->getMessage(),
				'trace' => $exception->getTrace(),
			], 400);
		}

		return response()->json([
			'message' => 'Тренировка изменена'
		], 400);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Training $training
	 * @return JsonResponse
	 */
    public function destroy(Training $training): JsonResponse
    {
        try {
        	$training->delete();
		} catch (Exception $exception) {
        	return response()->json([
        		'message' => 'Ошибка удаления Тренировки',
				'error' => $exception->getMessage(),
			], 400);
		}

		return response()->json([
			'message' => 'Тренировка удалена',
		], 200);
    }
}
