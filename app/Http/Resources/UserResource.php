<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        	'login' => $this->login,
			'email' => $this->email,
			'name' => $this->name,
			'gender' => $this->gender,
			'weight' => $this->weight,
			'height' => $this->height
		];
    }
}
