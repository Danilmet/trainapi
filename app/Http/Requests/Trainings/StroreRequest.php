<?php

namespace App\Http\Requests\Trainings;

use Illuminate\Foundation\Http\FormRequest;

class StroreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'exercises' => 'required|array'
        ];
    }

	/**
	 * @return array
	 */
	public function messages()
	{
		return [
			'required' => 'Не заполнено поле :attribute',
			'exercises' => 'Поле :attribute должно быть массивом'
		];
	}

	/**
	 * @return array
	 */
	public function attributes()
	{
		return [
			'name' => 'Назавние',
			'exercises' => 'Упражнения',
		];
	}
}
