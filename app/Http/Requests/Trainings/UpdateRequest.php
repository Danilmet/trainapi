<?php

namespace App\Http\Requests\Trainings;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() && $this->user()->can('update', $this->route('training'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'exercises' => 'required|array',
        ];
    }

	/**
	 * @return array
	 */
	public function messages()
	{
		return [
			'required' => 'Не заполнено поле :attribute',
			'array' => 'Поле :attribute должно быть массивом'
		];
	}

	/**
	 * @return array
	 */
	public function attributes()
	{
		return [
			'name' => 'Назавние',
			'exercises' => 'Упраженения',
		];
	}
}
