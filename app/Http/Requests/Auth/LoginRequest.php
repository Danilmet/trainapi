<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required',
			'password' => 'required',
        ];
    }

	/**
	 * @return array
	 */
	public function messages()
	{
		return [
			'required' => 'Не заполнено поле :attribute'
		];
    }

	/**
	 * @return array
	 */
	public function attributes()
	{
		return [
			'login' => 'Логин',
			'password' => 'Пароль',
		];
    }
}
