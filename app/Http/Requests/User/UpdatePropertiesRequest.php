<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePropertiesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'height' => 'required',
			'weight' => 'required'
        ];
    }

	/**
	 * @return array
	 */
	public function messages()
	{
		return [
			'required' => 'Не заполнено поле :attribute',
		];
	}

	/**
	 * @return array
	 */
	public function attributes()
	{
		return [
			'height' => 'Рост',
			'weight' => 'Вес',
		];
	}
}
