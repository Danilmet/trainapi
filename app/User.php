<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login',
		'email',
		'password',
		'api_token',
		'name',
		'gender',
		'weight',
		'height',
		'email_verified_at',
		'remember_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
		'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
	 * Relations
	 */

	/**
	 * @return HasMany
	 */
    public function trainings(): HasMany
	{
		return $this->hasMany(Training::class);
	}

	/**
	 * @return HasMany
	 */
	public function exercises(): HasMany
	{
		return $this->hasMany(Exercise::class);
	}
}
