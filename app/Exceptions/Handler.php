<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param Exception $exception
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
	 */
    public function render($request, Exception $exception)
    {
		/**
		 * Обработка ошибки валидатора
		 */
		if ($exception instanceof ValidationException)
			return response()->json([
				'message' => 'Неверно введены данные.',
				'errors' => $exception->validator->getMessageBag()
			], 422);

		/**
		 * Обработка ошибки аутентификации
		 */
		if ($exception instanceof AuthenticationException)
			return response()->json([
				'message' => 'Ошибка аутентификации',
				'error' => 'authentication_error',
			], 403);


		if ($exception instanceof AuthorizationException)
			return response()->json([
				'message' => 'Нет прав на выполнение данного действия',
				'error' => 'access_denied',
			], 403);

		/**
		 * Для того, чтобы API возвращало response вместо html страницы
		 */
		if ($request->is('api/*')) {
			if ($exception instanceof ModelNotFoundException)
				return response()->json(['message' => 'Не найдено.',], 404);

			if ($exception instanceof NotFoundHttpException)
				return response()->json([
					'message' => 'Not found',
				], 404);

			$response = [
				'message' => 'Ошибка сервера',
			];
			if (config('app.debug')) {
				$response['error'] = $exception->getMessage();
			}
			return response()->json($response, 500);
		}

        return parent::render($request, $exception);
    }
}
