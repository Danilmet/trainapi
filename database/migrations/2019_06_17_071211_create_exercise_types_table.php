<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('display_name');
        });

        Schema::table('exercises', function (Blueprint $table) {
        	$table->bigInteger('type_id')
				->unsigned();

        	$table->foreign('type_id')->references('id')->on('exercise_types');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('exercises', function (Blueprint $table) {
			$table->dropForeign(['type_id']);

			$table->dropColumn('type_id');
		});

        Schema::dropIfExists('exercise_types');
    }
}
