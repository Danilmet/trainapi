<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')
				->unsigned();
            $table->string('name');
            $table->string('description')
				->nullable()
				->default('');
            $table->string('approaches');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('exercises', function (Blueprint $table) {
			$table->dropForeign(['user_id']);
		});

        Schema::dropIfExists('exercises');
    }
}
