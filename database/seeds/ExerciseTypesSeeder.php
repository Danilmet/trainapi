<?php

use Illuminate\Database\Seeder;

class ExerciseTypesSeeder extends Seeder
{
	private $types = [
		[
			'name' => 'once',
			'display_name' => 'Однократно',
		],
		[
			'name' => 'multiple',
			'display_name' => 'Многократно',
		],
		[
			'name' => 'time',
			'display_name' => 'По времени',
		]
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->types as $type) {
        	DB::table('exercise_types')->insert($type);
		}
    }
}
