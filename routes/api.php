<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post(
	'/login',
	'AuthController@login'
);
Route::post(
	'/register',
	'AuthController@register'
);

Route::get(
	'/user',
	'UserController@index'
)
	->middleware('auth:api');

Route::put(
	'/properties',
	'UserController@updateProperties'
)
	->middleware('auth:api');

Route::apiResource(
	'/exercise/types',
	'ExerciseTypeController',
	[
		'only' => 'index',
	]
);

Route::apiResource(
	'/exercises',
	'ExerciseController'
)
	->middleware('auth:api');

Route::apiResource(
	'/trainings',
	'TrainingController'
)
	->middleware('auth:api');